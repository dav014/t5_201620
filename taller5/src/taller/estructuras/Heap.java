package taller.estructuras;

import java.util.ArrayList;

import taller.mundo.Pedido;

public class Heap<T  extends Comparable > implements IHeap<T>{

	private int size ;
	private ArrayList arreglo;
	
	
	public Heap()
	{
		
		size = 0;
		arreglo = new ArrayList();
		arreglo.add(null);
		
	}
	
	
	public void add(T pedidosRecibidos)
	{
		arreglo.add(pedidosRecibidos);
		siftUp();
		size ++;
	}
	
	/**
	 * Retorna pero no remueve el elemento máximo/mínimo del heap.
	 * @return T elemento 
	 */
	public T peek()
	{
		if (isEmpty()) return null;
		T ret = (T) arreglo.get(size);
		return ret;
	}
	
	/**
	 * Retorna el elemento máximo/mínimo luego de removerlo del heap.
	 * @return T El elemento máximo/mínimo del heap
	 */
	public T poll()
	{   if (isEmpty()) return null;
		T ret = (T) arreglo.get(size );
		arreglo.remove(size);
		size--;
		return ret;
	}
	
	/**
	 * Retorna el número de elementos en el heap
	 * @return size Número de elementos en el heap
	 */
	public int size()
	{
		return size;
	}
	
	/**
	 * Retorna true si el heap no tiene elementos; false de lo contrario.
	 * @return
	 */
	public boolean isEmpty(){
		
		 boolean retorno = false;
		if (size == 0)
		{
			retorno = true;
		}
		
		return retorno;
	}
	
	/**
	 * Mueve el último elemento arriba en el arbol, mientras que sea necesario.
	 * Es usado para restaurar la condición de heap luego de inserción.
	 */



	@Override
	public void siftUp() {
		// TODO Auto-generated method stub
		int k = size;
		
		while(k>1)
		{
			if((((Pedido) arreglo.get(k)).getPrecio()) <(((Pedido) arreglo.get(k/2)).getPrecio())){
				
			int s = k/2;
			T a = (T) arreglo.get(k);
			arreglo.add(k, arreglo.get(s));
			arreglo.remove(k+1);
			arreglo.add(s, a);
			k =k/2;
			System.out.println("entro");
			}
			else 
				{
				
				if(((Pedido)arreglo.get(k)).getPrecio()== ((Pedido)arreglo.get(k/2)).getPrecio())
					{
						if(((Pedido)arreglo.get(k)).getCercania() < ((Pedido)arreglo.get(k/2)).getCercania())
							{
								T a = (T) arreglo.get(k);
									arreglo.add(k, arreglo.get(k/2));
									arreglo.remove(k+1);
									arreglo.add(k/2, a);
									k =k/2;
									
							}
						k = k/2;
					}
				k = k/2;
				}
			
		}
	}


	@Override
	public void siftDown() {
		// TODO Auto-generated method stub
		int k = 1;
		while(2*k<= arreglo.size())
		{
			int j = 2*k;
			if(j<k && ((Pedido) arreglo.get(j)).compareTo(arreglo.get(j+1)) < 0 ) j++;
			if(((Pedido) arreglo.get(k)).compareTo(arreglo.get(j)) > 0) break;
			T a = (T) arreglo.get(k);
			arreglo.add(k, arreglo.get(j));
			arreglo.remove(k+1);
			arreglo.add(j, a);
			
		}
	}






	
	
}
