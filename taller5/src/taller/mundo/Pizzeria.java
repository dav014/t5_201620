package taller.mundo;

import taller.estructuras.Heap;
import taller.estructuras.IHeap;


public class Pizzeria<T> 
{	
	// ----------------------------------
    // Constantes
    // ----------------------------------
	
	/**
	 * Constante que define la accion de recibir un pedido
	 */
	public final static String RECIBIR_PEDIDO = "RECIBIR";
	/**
	 * Constante que define la accion de atender un pedido
	 */
	public final static String ATENDER_PEDIDO = "ATENDER";
	/**
	 * Constante que define la accion de despachar un pedido
	 */
	public final static String DESPACHAR_PEDIDO = "DESPACHAR";
	/**
	 * Constante que define la accion de finalizar la secuencia de acciones
	 */
	public final static String FIN = "FIN";
	
	// ----------------------------------
    // Atributos
    // ----------------------------------
	
	/**
	 * Heap que almacena los pedidos recibidos
	 */
	// TODO
	public Heap almacenarPedidos ;
	
	/**
	 * 
	 * Getter de pedidos recibidos
	 */
	// TODO 
	public Pedido pedidosRecibidos;
	
	
 	/** 
	 * Heap de elementos por despachar
	 */
	// TODO 
	
	public Heap elementosDespachar;
	
	/**
	 * Getter de elementos por despachar
	 */
	// TODO 
	public Pedido despachar;
	
	// ----------------------------------
    // Constructor
    // ----------------------------------

	/**
	 * Constructor de la case Pizzeria
	 */
	public Pizzeria()
	{
		//TODO
		despachar = null;
		pedidosRecibidos = null;
		almacenarPedidos = new Heap();
		elementosDespachar = new Heap();
		
	}
	
	// ----------------------------------
    // Métodos
    // ----------------------------------
	
	/**
	 * Agrega un pedido a la cola de prioridad de pedidos recibidos
	 * @param nombreAutor nombre del autor del pedido
	 * @param precio precio del pedido 
	 * @param cercania cercania del autor del pedido 
	 */
	public void agregarPedido(String nombreAutor, double precio, int cercania)
	{
		pedidosRecibidos = new Pedido(nombreAutor,precio,cercania );
		almacenarPedidos.add(pedidosRecibidos);
		
	}
	
	// Atender al pedido más importante de la cola
	
	/**
	 * Retorna el proximo pedido en la cola de prioridad o null si no existe.
	 * @return p El pedido proximo en la cola de prioridad
	 */
	public Pedido atenderPedido()
	{
		// TODO 
		Pedido retorno = null; 
		if(!almacenarPedidos.isEmpty()){
		retorno = (Pedido) almacenarPedidos.poll();
		}
		return  retorno;
	}

	/**
	 * Despacha al pedido proximo a ser despachado. 
	 * @return Pedido proximo pedido a despachar
	 */
	public Pedido despacharPedido()
	{
		// TODO 
		Pedido retorno = (Pedido) elementosDespachar.poll();
				
	    return  retorno;
	}
	
	 /**
     * Retorna la cola de prioridad de pedidos recibidos como un arreglo. 
     * @return arreglo de pedidos recibidos manteniendo el orden de la cola de prioridad.
     */
     public Pedido [] pedidosRecibidosList()
     {
        // TODO
    	
    	 Pedido[] retorno = new Pedido[almacenarPedidos.size()] ;
    	 int s  = almacenarPedidos.size();
    	System.out.println("" + s);
    	for (int i = 0; i < s ; i++ )
    	{
    		
    		Pedido aa = (Pedido) almacenarPedidos.poll();
    		
    		retorno[i] = aa;
    		
    	}
    	System.out.println("" + s);
    	for (int i = 0; i < s ; i++ )
    	{
    		almacenarPedidos.add(retorno[i]);
    		
    		
    	}
    	 
    	 
        return retorno;
     }
     
      /**
       * Retorna la cola de prioridad de despachos como un arreglo. 
       * @return arreglo de despachos manteniendo el orden de la cola de prioridad.
       */
     public Pedido [] colaDespachosList()
     {
         // TODO 
    	 Pedido[] retorno = new Pedido[elementosDespachar.size()];
     	
    	 int s = elementosDespachar.size() ;
     	for (int i = 0; i < s ; i++ )
     	{
     		
     		Pedido aa = (Pedido) elementosDespachar.poll();

     		retorno[i] = aa;
     		
     	}
     	for (int i = 0; i < s ; i++ )
    	{
    		elementosDespachar.add(retorno[i]);
    		
    		
    	}
    
     	 
     	 
         return retorno;
    	 
        
     }
}
